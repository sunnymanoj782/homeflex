import { Component, inject } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrl: './profile.component.css'
})
export class ProfileComponent {

  auth = inject(AuthService);
  name = "";
  userProfileImg = "";
  email = "";

  constructor() {
    // Retrieve the user profile data from sessionStorage
    const userProfileData = sessionStorage.getItem("loogedInUser");

    // Check if the data exists and is valid JSON
    if (userProfileData && userProfileData !== 'undefined') {
      const parsedData = JSON.parse(userProfileData);
      this.name = parsedData.name;
      this.userProfileImg = parsedData.picture;
      this.email = parsedData.email;
    }
  }

  singOut(){
    sessionStorage.removeItem("loogedInUser");
    this.auth.signOut();
  }

}