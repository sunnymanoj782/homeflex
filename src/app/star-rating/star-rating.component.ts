// star-rating.component.ts
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent {
  @Input() rating: number = 0;
  @Input() readonly: boolean = false;
  @Output() ratingChange: EventEmitter<number> = new EventEmitter<number>();

  stars: boolean[] = Array(5).fill(false);

  ngOnChanges(): void {
    this.updateStars();
  }

  updateStars(): void {
    this.stars = this.stars.map((_, i) => this.rating > i);
  }

  rate(rating: number): void {
    if (!this.readonly) {
      this.rating = rating;
      this.updateStars();
      this.ratingChange.emit(this.rating);
    }
  }
}
