import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skincraft',
  templateUrl: './skincraft.component.html',
  styleUrls: ['./skincraft.component.css']
})
export class SkincraftComponent  {
  
  constructor() { }

  sendMessage() {
    const name = (document.getElementById('name') as HTMLInputElement).value;
    const phone = (document.getElementById('phone') as HTMLInputElement).value;
    const date = (document.getElementById('date') as HTMLInputElement).value;
    const message = `Hi, my name is ${name}. I would like to book an appointment on ${date}. My contact number is ${phone}.`;

    const whatsappURL = `https://api.whatsapp.com/send?phone=8897091929&text=${encodeURIComponent(message)}`;
    window.open(whatsappURL, '_blank');
  }
}
