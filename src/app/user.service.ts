import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'; // Import HttpClient
import { BehaviorSubject, Observable } from 'rxjs';

import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// Make sure you have created a User model

@Injectable({
  providedIn: 'root'
})
export class UserService {
  username: any;
  loginStatus: boolean;
  isUserLogged: BehaviorSubject<boolean>;
  login: any;

  constructor(private http: HttpClient) {
    this.loginStatus = false;
    this.isUserLogged = new BehaviorSubject<boolean>(false);
  }

  getIsUserLoggedStatus(): Observable<boolean> {
    return this.isUserLogged.asObservable();
  }

  getLoginStatus(): boolean {
    return this.loginStatus;
  }

  setLoginStatus(): void {
    this.loginStatus = true;
    this.isUserLogged.next(true);
  }

  setLogoutStatus(): void {
    this.loginStatus = false;
    this.isUserLogged.next(false);
  }

  registerUser(registerForm: any): Observable<any> {
    console.log('Request Body:', registerForm);
    const body = {
      userEmail: registerForm.email,
      userName: registerForm.username,
      userPassword: registerForm.userPassword,
      userPhone: registerForm.userPhone ? registerForm.userPhone.toString() : '', // Handle undefined userPhone
      confirmPassword: registerForm.confirmPassword
    };

    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post('http://localhost:8085/users/register', body, { headers });
  }

  userLogin(loginForm: any): Observable<any> {
    const { emailId, password } = loginForm;
    const url = `http://localhost:8085/users/login/${emailId}/${password}`;
    return this.http.get(url);
  }

  resetPassword(email: string, newPassword: string): Observable<any> {
    const data = { email, newPassword };
    return this.http.post('http://localhost:8085/users/changepassword', data);
  }

  getAllUsers(): Observable<any> {
    return this.http.get('http://localhost:8085/users/getAllUsers');
  }

  deleteUserById(userId: number): Observable<any> {
    const url = `http://localhost:8085/users/deleteUserById/${userId}`;
    return this.http.delete(url, { responseType: 'text' });
  }

  updateUser(user: any): Observable<any> {
    const updateUrl = `http://localhost:8085/users/update/${user.id}`;
    console.log('Updated URL:', updateUrl);
    console.log('Data to be sent:', user);
    return this.http.put(updateUrl, user);
  }
}
