import { Component } from '@angular/core';

@Component({
  selector: 'app-cardcarousel',
  templateUrl: './cardcarousel.component.html',
  styleUrls: ['./cardcarousel.component.css']
})
export class CardcarouselComponent {
  images = [
    { url: 'images/cooking-maid.jpg',              caption: 'Cooking Maid' },
    { url: 'images/hair-studio-for-women.jpg',     caption: 'Hair Studio For Women' },
    { url: 'images/tattoo-designer.jpg',           caption: 'Tattoo Designer' },
    { url: 'images/personal-fashion-designer.jpg', caption: 'Personal Fashion Designer' },
    { url: 'images/laser-tattoo-removal.jpg',      caption: 'Laser Tattoo Removal' },
    { url: 'images/daycare-maid.jpg',              caption: 'Daycare Maid' },
    { url: 'images/laser-hair-removal.jpg',        caption: 'Laser Hair Removal' },
    { url: 'images/color-analysis.jpg',            caption: 'Color Analysis' }
  ];
  currentIndex = 0;

  next() {
    if (this.currentIndex < this.images.length - 4) {
      this.currentIndex++;
    }
    this.updateCarousel();
  }

  prev() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
    this.updateCarousel();
  }

  updateCarousel() {
    const slideWidth = 25; // Width percentage of each slide
    const transformValue = -1 * this.currentIndex * slideWidth;
    const translateXValue = `translateX(${transformValue}%)`;
    document.querySelector('.carousel')!.setAttribute('style', `transform: ${translateXValue}`);
  }
} 