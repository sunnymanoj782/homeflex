import { Component } from '@angular/core';
import emailjs, { EmailJSResponseStatus } from 'emailjs-com';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent {

  constructor() {
    emailjs.init('rUTGB7ZRcRC_lQPvC'); // Initialize EmailJS with your User ID (public key)
  }

  sendEmail(e: Event) {
    e.preventDefault();
    // Use the EmailJS SDK to send the form data
    emailjs.sendForm('service_9fko1ag', 'template_1hi8aqk', e.target as HTMLFormElement, 'rUTGB7ZRcRC_lQPvC')
      .then((response: EmailJSResponseStatus) => {
        console.log('Email sent successfully:', response);
        alert('Email sent successfully!');
        // Optionally reset the form after successful submission
        (document.getElementById('contactForm') as HTMLFormElement).reset();
      }, (error) => {
        console.error('Failed to send email:', error);
        alert('Failed to send email. Please try again later.');
      });
  }
}
