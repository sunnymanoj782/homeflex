// reviews.component.ts
import { Component, OnInit } from '@angular/core';


interface Review {
  rating: number;
  comment: string;
}

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
  reviews: Review[] = [];
  newReview: Review = { rating: 5, comment: '' };

  ngOnInit(): void {
    // Load reviews from local storage if available
    const storedReviews = localStorage.getItem('reviews');
    if (storedReviews) {
      this.reviews = JSON.parse(storedReviews);
    }
  }

  addReview(): void {
    // Add the new review
    this.reviews.push({ ...this.newReview });

    // Save reviews to local storage
    localStorage.setItem('reviews', JSON.stringify(this.reviews));

    // Reset the form
    this.newReview = { rating: 5, comment: '' };
  }
}
