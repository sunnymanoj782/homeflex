import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maidsupply',
  templateUrl: './maidsupply.component.html',
  styleUrls: ['./maidsupply.component.css']
})
export class MaidsupplyComponent implements OnInit {
  public selectedindex: number = 0;
  public images = [
    './images/maid1.png',
    './images/maid2.png',
    './images/maid3.png',
    './images/maid4.png'
    // Add more images as needed
  ];

  constructor() { }

  ngOnInit(): void {
    // Start automatic slideshow
    this.showSlides();
  }

  // Function to show slides automatically
  showSlides() {
    setInterval(() => {
      this.selectedindex = (this.selectedindex + 1) % this.images.length;
    }, 2000); // Change slide every 2 seconds
  }

  // Function to select a specific image (dot navigation)
  selectImage(index: number) {
    this.selectedindex = index;
  }
  
  
 

  sendMessage() {
    const name = (document.getElementById('name') as HTMLInputElement).value;
    const phone = (document.getElementById('phone') as HTMLInputElement).value;
    const date = (document.getElementById('date') as HTMLInputElement).value;
    const message = `Hi, my name is ${name}. I would like to book an appointment on ${date}. My contact number is ${phone}.`;

    const whatsappURL = `https://api.whatsapp.com/send?phone=918897091929&text=${encodeURIComponent(message)}`;
    window.open(whatsappURL, '_blank');
  }
}
 
