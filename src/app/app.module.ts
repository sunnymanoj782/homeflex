import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AboutComponent } from './pages/about/about.component';
import { TermsConditionsComponent } from './pages/terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { AntiDiscriminationPolicyComponent } from './pages/anti-discrimination-policy/anti-discrimination-policy.component';
import { UcImpactComponent } from './pages/uc-impact/uc-impact.component';
import { CareersComponent } from './pages/careers/careers.component';
import { UcReviewsComponent } from './pages/uc-reviews/uc-reviews.component';
import { CategoriesNearYouComponent } from './pages/categories-near-you/categories-near-you.component';
import { BlogComponent } from './pages/blog/blog.component';
import { RegisterProfessionalComponent } from './pages/register-professional/register-professional.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { EmailverificationComponent } from './emailverification/emailverification.component';
import { SetnewpasswordComponent } from './setnewpassword/setnewpassword.component';
import { SkincraftComponent } from './skincraft/skincraft.component';
import { FashionAdvisingComponent } from './fashion-advising/fashion-advising.component';
import { DaycareComponent } from './daycare/daycare.component';
import { MaidsupplyComponent } from './maidsupply/maidsupply.component';
import { BooknowComponent } from './booknow/booknow.component';
import { CardcarouselComponent } from './cardcarousel/cardcarousel.component';
import { Cardcarouselcontainer2Component } from './cardcarouselcontainer2/cardcarouselcontainer2.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { SalonformenComponent } from './salonformen/salonformen.component';
import { SalonforwomenComponent } from './salonforwomen/salonforwomen.component';
import { NativewaterpurifyComponent } from './nativewaterpurify/nativewaterpurify.component';
import { PaintingComponent } from './painting/painting.component';
import { AcComponent } from './ac/ac.component';
import { ProfileComponent } from './profile/profile.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    AboutUsComponent,
    ContactUsComponent,
    AboutComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent,
    AntiDiscriminationPolicyComponent,
    UcImpactComponent,
    CareersComponent,
    UcReviewsComponent,
    CategoriesNearYouComponent,
    BlogComponent,
    RegisterProfessionalComponent,
    ForgotpasswordComponent,
    EmailverificationComponent,
    SetnewpasswordComponent,
    SkincraftComponent,
    FashionAdvisingComponent,
    DaycareComponent,
    MaidsupplyComponent,
    BooknowComponent,
    CardcarouselComponent,
    Cardcarouselcontainer2Component,
    ReviewsComponent,
    StarRatingComponent,
    SalonformenComponent,
    SalonforwomenComponent,
    NativewaterpurifyComponent,
    PaintingComponent,
    AcComponent,
    ProfileComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    HttpClientModule, // Add HttpClientModule here
    ReactiveFormsModule, // Add ReactiveFormsModule here
    BrowserAnimationsModule, // Add BrowserAnimationsModule for Toastr
    ToastrModule.forRoot(),
    NgxCaptchaModule,
    AppRoutingModule,
    NgxCaptchaModule,
      // Use the routing module

  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
