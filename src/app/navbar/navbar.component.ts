

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service'; // Adjust path as per your file structure

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

 
}

