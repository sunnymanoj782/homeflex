import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomeComponent } from './home/home.component';
import { TermsConditionsComponent } from './pages/terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { AntiDiscriminationPolicyComponent } from './pages/anti-discrimination-policy/anti-discrimination-policy.component';

import { CareersComponent } from './pages/careers/careers.component';

import { CategoriesNearYouComponent } from './pages/categories-near-you/categories-near-you.component';
import { BlogComponent } from './pages/blog/blog.component';
import { RegisterProfessionalComponent } from './pages/register-professional/register-professional.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { EmailverificationComponent } from './emailverification/emailverification.component';
import { SetnewpasswordComponent } from './setnewpassword/setnewpassword.component';
import { SkincraftComponent } from './skincraft/skincraft.component';
import { FashionAdvisingComponent } from './fashion-advising/fashion-advising.component';
import { DaycareComponent } from './daycare/daycare.component';
import { MaidsupplyComponent } from './maidsupply/maidsupply.component';
import { SalonforwomenComponent } from './salonforwomen/salonforwomen.component';
import { SalonformenComponent } from './salonformen/salonformen.component';
import { PaintingComponent } from './painting/painting.component';
import { AcComponent } from './ac/ac.component';
import { NativewaterpurifyComponent } from './nativewaterpurify/nativewaterpurify.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'terms-conditions', component: TermsConditionsComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'anti-discrimination-policy', component: AntiDiscriminationPolicyComponent },
  { path: 'careers', component: CareersComponent },
  { path: 'categories-near-you', component: CategoriesNearYouComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'register-professional', component: RegisterProfessionalComponent },
  { path: 'forget-password', component: ForgotpasswordComponent },
  { path: 'email-verification', component: EmailverificationComponent },
  { path: 'set-new-password', component: SetnewpasswordComponent },
  { path: 'skincraft', component: SkincraftComponent }, 
  { path: 'fashion-advising', component: FashionAdvisingComponent },
  { path: 'daycare', component: DaycareComponent },
  { path: 'maidsupply', component: MaidsupplyComponent },
  { path: 'salonforwomen', component: SalonforwomenComponent }, 
  { path: 'salonformen', component: SalonformenComponent },
  { path: 'painting', component: PaintingComponent },
  { path: 'ac', component: AcComponent },
  { path: 'nativewaterpurify', component: NativewaterpurifyComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'home', component: HomeComponent, pathMatch: 'full' } // Home route

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
