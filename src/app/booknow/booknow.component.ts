
import { Component } from '@angular/core';

@Component({
  selector: 'app-booknow',
  templateUrl: './booknow.component.html',
  styleUrls: ['./booknow.component.css']
})
export class BooknowComponent {
  name: string = '';
  phone: string = '';
  date: string = '';
  time: string = '';
  errorMessage: string = '';

  constructor() { }

  sendMessage() {
    const currentDate = new Date();
    const selectedDate = new Date(this.date);
    const selectedTime = this.time.split(':').map(Number); // Split time into hours and minutes
    const currentDateString = currentDate.toISOString().split('T')[0];

    // Validation for date
    if (selectedDate < currentDate) {
      this.errorMessage = 'Invalid date, please check the date';
      return;
    } else {
      this.errorMessage = '';
    }

    // Validation for time
    if (this.date === currentDateString) {
      if (selectedTime[0] < currentDate.getHours() || (selectedTime[0] === currentDate.getHours() && selectedTime[1] < currentDate.getMinutes())) {
        this.errorMessage = 'Invalid time, please check the time';
        return;
      } else {
        this.errorMessage = '';
      }
    }

    const message = `Hi, my name is ${this.name}. I would like to book an appointment on ${this.date}. My contact number is ${this.phone}. My slot booking time is ${this.time}`;
    const whatsappURL = `https://api.whatsapp.com/send?phone=918897091929&text=${encodeURIComponent(message)}`;
    window.open(whatsappURL, '_blank');
  }
}

