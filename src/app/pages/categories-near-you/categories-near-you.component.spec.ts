import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesNearYouComponent } from './categories-near-you.component';

describe('CategoriesNearYouComponent', () => {
  let component: CategoriesNearYouComponent;
  let fixture: ComponentFixture<CategoriesNearYouComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CategoriesNearYouComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoriesNearYouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
