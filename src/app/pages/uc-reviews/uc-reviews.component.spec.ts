import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UcReviewsComponent } from './uc-reviews.component';

describe('UcReviewsComponent', () => {
  let component: UcReviewsComponent;
  let fixture: ComponentFixture<UcReviewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UcReviewsComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UcReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
