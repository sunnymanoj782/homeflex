import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UcImpactComponent } from './uc-impact.component';

describe('UcImpactComponent', () => {
  let component: UcImpactComponent;
  let fixture: ComponentFixture<UcImpactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UcImpactComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UcImpactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
