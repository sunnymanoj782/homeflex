import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';

declare var google:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public aFormGroup!: FormGroup;  
  public sitekey: string = "6LdxW_8pAAAAAE11Uff4hVnZIz1VPEmyCUWi2mmT"; // Your reCAPTCHA site key
  user: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private userService: UserService
  ) {
    this.user = {
      emailId: '',
      password: ''
    };
  }

  ngOnInit() {

    google.accounts.id.initialize({
      client_id:'264914900793-qv0nc5g9feikdbnr77qi6n2t4sbap3r4.apps.googleusercontent.com',
      callback: (resp: any)=>this.handleLogin(resp)

    });

    google.accounts.id.renderButton(document.getElementById("google-btn"),{
      theme:'filled_blue',
      size: 'large',
      shape: 'rectangle',
      width: 350,
    });


    this.aFormGroup = this.formBuilder.group({
      emailId: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      recaptcha: ['', Validators.required]
    });
  }

  private decodeToken(token: string){
    return JSON.parse(atob(token.split(".")[1]));
  }

  handleLogin(response:any){
    if(response){
      //decode the token
      const payLoad = this.decodeToken(response.credential);
      //store in session
      sessionStorage.setItem("loogedInUser",JSON.stringify(payLoad))
      //navigae to home
      this.router.navigate(['home'])

    }
  }

  loginSubmit(loginForm: any) {
    localStorage.setItem("emailId", loginForm.emailId);

    if (loginForm.emailId == 'admin@gmail.com' && loginForm.password == 'admin') {
      this.userService.setLoginStatus();
      this.router.navigate(['admin']);
      this.toastr.success('Successfully logged in as Admin', 'Success'); // Admin login success message
    } else {
      this.user.emailId = loginForm.emailId;
      this.user.password = loginForm.password;

      this.userService.userLogin(this.user).subscribe(
        data => {
          console.log(data);
          if (data) {
            this.userService.setLoginStatus();
            this.router.navigate(['home']);
            this.toastr.success('Successfully logged in !!', 'Success');
          } else {
            this.toastr.error('Login Unsuccessful', 'Error');
          }
        },
        error => {
          console.log('Service Error:', error);
          this.toastr.error('Login Unsuccessful', 'Error');
        }
      );
    }
  }

  handleReset() {
    console.log('reCAPTCHA reset');
  }

  handleExpire() {
    console.log('reCAPTCHA expired');
  }

  handleLoad() {
    console.log('reCAPTCHA loaded');
  }

  handleSuccess(event: any) {
    console.log('reCAPTCHA success', event);
  }
}
