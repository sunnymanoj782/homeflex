import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import * as CryptoJS from 'crypto-js';
import { UserService } from '../user.service'; // Import UserService

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  
  registerForm: FormGroup;
  passwordCriteriaMessage: string = '';

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private userService: UserService // Inject UserService
  ) {
    this.registerForm = this.fb.group({
      username: ['', [Validators.required, Validators.pattern(/^[a-zA-Z][a-zA-Z0-9]*$/)]], // Username starts with alphabet
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, this.passwordValidator()]],
      confirmPassword: ['', Validators.required],
      terms: [false, Validators.requiredTrue],
      userPhone: ['', [Validators.required, Validators.pattern(/^[0-9]{10}$/)]] // Ensure userPhone is included
    }, { validators: this.passwordMatchValidator });
  }

  passwordMatchValidator(control: AbstractControl) {
    const password = control.get('password');
    const confirmPassword = control.get('confirmPassword');
    if (password && confirmPassword && password.value !== confirmPassword.value) {
      control.get('confirmPassword')?.setErrors({ mismatch: true });
    } else {
      control.get('confirmPassword')?.setErrors(null);
    }
  }

  passwordValidator() {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const value = control.value as string;
      if (!value) {
        this.passwordCriteriaMessage = 'Password is required (min 8 characters)';
        return { 'invalidPassword': true };
      }

      // Password criteria
      const minLength = 8; // Minimum length
      const hasUppercase = /[A-Z]/.test(value); // At least one uppercase letter
      const hasLowercase = /[a-z]/.test(value); // At least one lowercase letter
      const hasNumber = /\d/.test(value); // At least one number
      const hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(value); // At least one special character

      // Check if all criteria are met
      const isValid = hasUppercase && hasLowercase && hasNumber && hasSpecialChar && value.length >= minLength;

      if (!isValid) {
        // Build error message based on failed criteria
        this.passwordCriteriaMessage = 'Password must';
        this.passwordCriteriaMessage += !hasUppercase ? ' contain at least one uppercase letter,' : '';
        this.passwordCriteriaMessage += !hasLowercase ? ' contain at least one lowercase letter,' : '';
        this.passwordCriteriaMessage += !hasNumber ? ' contain at least one number,' : '';
        this.passwordCriteriaMessage += !hasSpecialChar ? ' contain at least one special character,' : '';
        this.passwordCriteriaMessage += ` be at least ${minLength} characters long.`;

        return { 'invalidPassword': true };
      }

      this.passwordCriteriaMessage = '';
      return null;
    };
  }

  encryptPassword(password: string): string {
    return CryptoJS.AES.encrypt(password.trim(), 'secret-key').toString();
  }

  onSubmit() {
    if (this.registerForm.valid) {
      const formValue = this.registerForm.value;
      const encryptedPassword = this.encryptPassword(formValue.password);
      const user = { ...formValue, userPassword: encryptedPassword };

      this.userService.registerUser(user).subscribe(
        response => {
          this.toastr.success('Thank you for registering, you registered successfully', 'Success');
          this.router.navigate(['/login']);
        },
        error => {
          this.toastr.error('Registration failed', 'Error');
          console.error(error);
        }
      );
    } else {
      this.toastr.error('Please fill out the form correctly.', 'Error');
    }
  }

  // Getter methods for easier access in the template
  get username() { return this.registerForm.get('username'); }
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('password'); }
  get confirmPassword() { return this.registerForm.get('confirmPassword'); }
  get terms() { return this.registerForm.get('terms'); }
  get userPhone() { return this.registerForm.get('userPhone'); }
}
